#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*Lista*/
typedef void* objeto;

typedef unsigned char byte;

typedef struct no
{
	objeto o;
	struct no *prox;	
}no;

typedef struct lista
{
	int tam;
	no *head;
}lista;

lista *constroiLista()
{
	lista * l = (lista *)malloc(sizeof(lista));
	if(!l){ printf("lista nao criada\n");return NULL;}

	l->tam = 0;
	l->head=NULL;
	return l;
}

no *constroiNo(objeto o)
{
	no * n = (no *)malloc(sizeof(no));	
	if(!n){ 
		printf("No nao criado\n");return NULL;
	}
	n->o = o;
	n->prox=NULL;
	return n;	
}

int tamLista(lista *l)
{
	return l->tam;
}

void insereFrenteLista(lista *l,objeto o) 	//PUSH
{
	no* p=constroiNo(o);
	p->prox = l->head;
	l->head = p;
	l->tam=l->tam+1;
}

void insereFinalLista(lista *l,objeto o)   //QUEUE
{
	int tam = l->tam;
	if(tam==0)	
	{
		insereFrenteLista(l,o);
		return;
	}
	int i;
	no *aux = l->head;
	no* p=constroiNo(o);
	for(i=0;i<tam-1;i++)
		aux=aux->prox;
	aux->prox=p;
	l->tam=l->tam+1;
	return;
}

objeto removeFrenteLista(lista *l)			//POP
{

	no *aux = l->head;

//	printf("%d\n",l->tam);
	objeto o = aux->o;

	l->head = aux->prox;
	l->tam=l->tam-1;

	free(aux);	

	return o;
}

objeto removeFinalLista(lista *l,objeto o)   //deQUEUE
{
	return NULL;
}

objeto removeObjetoLista(lista *l,objeto o)   //deQUEUE
{
	if(l->tam==0)
		return NULL;
	no *aux=l->head;

	if(aux->o==o)
	{
		removeFrenteLista(l);
		return o;
	}
	for(aux;aux;aux=aux->prox)
	{
		if(aux->prox)
		{
			if(aux->prox->o==o)
			{
				no *prox=(aux->prox)->prox;
				free(aux->prox);
				aux->prox=prox;
				l->tam=l->tam-1;
				return o;				
			}
		}
	}	
	return NULL;
}

no *buscaNoLista(lista *l,objeto o)
{
	no*	aux=l->head;
	for(aux;aux;aux=aux->prox)
		if(aux->o==o)
			return aux;
	return NULL;
}

void insereListaAntesDe(lista *l,objeto o,objeto antigo)
{
	no *achado = buscaNoLista(l,o);
	if(!achado)
	{
		no *aux = l->head;
		if(aux->o==antigo)
		{
			insereFrenteLista(l,o);
			return;
		}

		for(aux;aux;aux=aux->prox)
		{
			if(aux->prox)
			{
				if(aux->prox->o==antigo)
				{
					no *pas  = aux->prox;
					no *novo = constroiNo(o);
					aux->prox=novo;
					novo->prox=pas;
					l->tam=l->tam+1;
					return;				
				}
			}
		}
	}
	else
	{
		removeObjetoLista(l,o);
		insereListaAntesDe(l,o,antigo);
	}
}

void destroiLista(lista *l)
{
	if(l->head==NULL)
		return;
	no *aux = l->head;
	l->head = aux->prox;
	l->tam = l->tam-1;
	free(aux);
	destroiLista(l);
}

typedef struct par
{
	int u;
	int v;
}par;

typedef struct grafo
{
	/*Definicao Formal*/
	int vTam;
	int eTam;
	lista **E; 
	/*Subgrafo Induzido
	lista *vInd;
	par   *eInd;
	/*Ciclo
	lista **ciclos*/
}grafo;

grafo *constroiGrafo()
{	
	int i,V,E;
	scanf("%d",&V);
	scanf("%d",&E);

	grafo *g = (grafo*)malloc(sizeof(grafo));
	g->vTam=V; 
	g->eTam=E;
	g->E = (lista **)malloc(V*sizeof(lista*));

	/*Uma lista de adjacencia para cada vertice*/
	for(i=0;i<V;i++)
		g->E[i]=(lista*)constroiLista();
	
	for(i=0;i<E;i++)
	{
		int a,b;
		scanf("%d",&a);
		scanf("%d",&b);
		insereFrenteLista(g->E[a],(objeto)b);
		insereFrenteLista(g->E[b],(objeto)a);
	}	
	return g;
}

void mostraGrafo(grafo *g)
{
	int i;no *aux;
	printf("Mostrando Grafo:\n");
	printf("  	g->vTam = %d\n",g->vTam);
	printf("	g->eTam = %d\n",g->eTam);
	int edgeCont=0;
	for(i=0;i<g->vTam;i++)
	{
		aux=g->E[i]->head;
		for(aux;aux!=NULL;aux=aux->prox)	
		{
			printf("	  %d %d\n",i,aux->o);
			edgeCont++;
			if(edgeCont==g->eTam)
				break;
		}
		if(edgeCont==g->eTam)
				break;
		
	}
}

int existeAresta(grafo *g,int u,int v)
{
	no *aux = g->E[u]->head;
	for(aux;aux;aux=aux->prox)
		if(aux->o==v)
			return 1;
	return 0;
}

int vizinhosFormamCliqueComRemocao(grafo *g,int vert,lista *removidos)
{
	int i=0;int j;
	int tam = tamLista(g->E[vert]);
	//if(tam==1)
		//return 1;

	int *array = malloc(sizeof(int)*tam);

	no *aux = g->E[vert]->head;
	for(aux;aux;aux=aux->prox)
	{
		if(!buscaNoLista(removidos,aux->o))
		{ array[i++]=aux->o; }
		else 
			tam--; 
	}

	for(i=0;i<tam;i++)
	{
		for(j=i+1;j<tam;j++)
		{
			if(!existeAresta(g,array[i],array[j]))
			{
				free(array);
				return 0;
			}
		}
	}
	free(array);
	return 1;
}

int verticesFormamClique(grafo *g,lista *vertices)
{
	int i=0;
	int tam = tamLista(vertices);

	int ligados;
	int qtVizinhos;
	int v,u;
	no *aux=vertices->head;
	no *auxEdge;
	for(aux;aux;aux=aux->prox)
	{
		v=aux->o;
		qtVizinhos = g->E[v]->tam;
		if(qtVizinhos>=tam-1)
		{
			ligados = 0;
			auxEdge = g->E[v]->head;
			for(auxEdge;auxEdge;auxEdge=auxEdge->prox)
			{
				u=auxEdge->o;
				if(buscaNoLista(vertices,u))
					ligados++;
			}
			if(ligados!=tam-1)
				return 0;
		}
		else 
			return 0;
	}	
	
	return tam;
}



int vizinhosFormamClique(grafo *g,int vert)
{
	int i=0;int j;
	int tam = tamLista(g->E[vert]);
	//if(tam==1)
		//return 1;

	int *array = malloc(sizeof(int)*tam);

	no *aux = g->E[vert]->head;
	for(aux;aux;aux=aux->prox)
		array[i++]=aux->o;

	for(i=0;i<tam;i++)
	{
		for(j=i+1;j<tam;j++)
		{
			if(!existeAresta(g,array[i],array[j]))
			{
				free(array);
				return 0;
			}
		}
	}
	free(array);
	return 1;
}

int verticeSimplicialComRemocao(grafo *g,int vert,lista *removidos)
{
	if(vizinhosFormamCliqueComRemocao(g,vert,removidos))
		return 1;
	else
		return 0;
}

int verticeSimplicial(grafo *g,int vert)
{
	if(vizinhosFormamClique(g,vert))
		return 1;
	else
		return 0;
}

lista *constroiListaComVertices(grafo *g)
{
	lista *l = constroiLista();
	int i=0;
	for(i=0;i<g->vTam;i++)	
		insereFrenteLista(l,i);
	return l;	
}

int removeArbitrario(lista *l)
{
	if(tamLista(l)>0)
		return	removeFrenteLista(l);
	//printf("Lista Vazia\n");
	return -1;
}

lista * contidoEmAlgumConjunto(lista *listaDeConjuntos,int w)
{
	no *aux;
	lista *l;
	no *S = listaDeConjuntos->head;
	for(S;S;S=S->prox)
	{
		l = S->o;
		if(buscaNoLista(l,w))
			return l;
	}
	return NULL;
}
void mostraLista(lista *l)
{
	if(l->tam==0)
	{
		//printf("lista vazia\n");
		return;
	}
	no *aux = l->head;
	for(aux;aux;aux=aux->prox)
		printf("%d ",aux->o);
	printf("\n");
}

lista *inicial=NULL;
void mostraListaDeConjuntos(lista *l)
{
	/*printf("Mostrando Lista de Conjuntos com tam %d\n",l->tam);
	no *aux = l->head;
	for(aux;aux;aux=aux->prox)
	{
		if(aux->o==inicial)
			{mostraLista(aux->o); printf("Lista Inicial ->");}
		else mostraLista(aux->o);
	}*/
}

lista *lexBFS(grafo *g)
{
	lista *ordem=constroiLista();lista *listaDeConjuntos=constroiLista();
	lista *conjuntoInicial = constroiListaComVertices(g);
	int v;int w;lista *l;lista *S,*T;inicial = conjuntoInicial;
	insereFrenteLista(listaDeConjuntos,conjuntoInicial);
	while(tamLista(listaDeConjuntos)>0)
	{
		mostraListaDeConjuntos(listaDeConjuntos);
		l = listaDeConjuntos->head->o;
		v=removeArbitrario(l);	
		if(tamLista(l)==0){
			removeFrenteLista(listaDeConjuntos);
			free(l);
		}
		//printf("vertice Escolhido = %d\n",v);
		insereFrenteLista(ordem,v);
		no *aux=g->E[v]->head;
		lista *conjuntosGerenciadosPorV = constroiLista();
		for(aux;aux;aux=aux->prox)
		{	
			w=aux->o;//printf("vizinho %d de %d\n",w,v);
			S = contidoEmAlgumConjunto(listaDeConjuntos,w); 
			if(S){
				//printf("	Vizinho encontrado na lista -> ");//mostraLista(S);
				no *resultado=buscaNoLista(conjuntosGerenciadosPorV,S);
				if(!resultado){
					//printf("	Lista Não Gerenciada por %d\n",v);
					T=constroiLista();
					insereFrenteLista(conjuntosGerenciadosPorV,S);
					insereListaAntesDe(listaDeConjuntos,T,S);
				}
				else{	
					//T=resultado->o;//printf("	Lista Ja gerenciada por %d\n",v);
					insereListaAntesDe(listaDeConjuntos,T,S);
				}
				removeObjetoLista(S,w);insereFrenteLista(T,w);
				if(tamLista(S)==0){
					removeObjetoLista(listaDeConjuntos,S);free(S);
				}
			}
			else{
				//printf("Visinho não encontrado em nenhuma Lista do conjunto\n");
			}
		}
		destroiLista(conjuntosGerenciadosPorV);
	}
	return ordem;
}

int cordal(grafo *g,lista *ordem)
{
	lista *removidos=constroiLista();

	no *aux=ordem->head;
	for(aux;aux;aux=aux->prox)
	{
		if(!verticeSimplicialComRemocao(g,aux->o,removidos))
			return 0;
		insereFrenteLista(removidos,aux->o);	
	}
	return 1;
}

int buscaCliqueMaxima(grafo *g)
{
	//return 
	return 0;
}

lista *achaIntersecao(lista *K,lista *vizinhanca)
{
	
	lista *intersecao = constroiLista();
	
	no *vizinho = vizinhanca->head;
	no *aux=NULL;
	int v;
	for(vizinho;vizinho;vizinho=vizinho->prox)
	{
		v=vizinho->o;
		aux = K->head;
		for(aux;aux;aux=aux->prox)
		{
			if((aux->o==v)&&(!buscaNoLista(intersecao,v)))
				insereFrenteLista(intersecao,v);
		}
	}

	return intersecao;
}

lista *achaUniao(lista *K,lista *vizinhanca)
{
	int v;
	lista *uniao = constroiLista();	

	no *vizinho = vizinhanca->head;
	for(vizinho;vizinho;vizinho=vizinho->prox)
	{
		v=vizinho->o;
		if(!buscaNoLista(uniao,v))
			insereFrenteLista(uniao,v);
	}

	no *aux = K->head;
	for(aux;aux;aux=aux->prox)
	{
		v=aux->o;
		if(!buscaNoLista(uniao,v))
			insereFrenteLista(uniao,v);
	}

	return uniao;
}

lista *achaUniao2(lista *K,int vert)
{
	int v;
	lista *uniao = constroiLista();	

	no *aux = K->head;
	for(aux;aux;aux=aux->prox)
	{
		v=aux->o;
		if(!buscaNoLista(uniao,v))
			insereFrenteLista(uniao,v);
	}

	if(!buscaNoLista(uniao,vert))
		insereFrenteLista(uniao,vert);
	return uniao;
}


int escolheVerticePertencenteK(lista *K)
{
	no *aux = K->head;
	return aux->o;	
}

lista *copiaLista(lista *Q)
{
	int v;
	lista *l = constroiLista();
	no *aux = Q->head;
	for(aux;aux;aux=aux->prox)
	{
		v=aux->o;
		insereFrenteLista(l,v);
	}
	return l;
}

lista *cqMaxima=NULL;
int cqMaximaTam = -1;

int particaoFormaBiclique(grafo *g,lista *C,lista *c)
{
	if((C->tam==0)||(c->tam==0))
		return 0;
	int edgeTam = 0;
	no *aux = C->head;
	no *aux2 = c->head;
	int v,u;
	for(aux;aux;aux=aux->prox)
	{
		v=aux->o;
		lista *vizinhosDeV = g->E[v];
		for(aux2=c->head;aux2;aux2=aux2->prox)
		{
			u=aux2->o;
			if(!buscaNoLista(vizinhosDeV,u))
				return -1;
		}
	}
	return C->tam*c->tam;
}

int minBiClique=-1;
int maxBiClique=-1;
lista *p1 = NULL;
lista *p2 = NULL;

void constroiBiParticoes(grafo *g,lista *C,lista *c)
{
	//printf("Entrou\n");
	if(C->tam==0)
		return;
	int max = particaoFormaBiclique(g,C,c);
	if(max>maxBiClique)
	{
		maxBiClique = max;
		p1 = copiaLista(C);
		p2 = copiaLista(c);
	}
	int v;
	lista *nC = copiaLista(C);
	lista *nc = copiaLista(c);
	no *aux = C->head;
	for(aux;aux;aux=aux->prox)
	{
		v = aux->o;
		removeObjetoLista(nC,v);
		insereFrenteLista(nc,v);		
		if( (nC->tam*nc->tam) >= minBiClique)
			constroiBiParticoes(g,nC,nc);
		if( (nC->tam*c->tam) >= minBiClique)
			constroiBiParticoes(g,nC,c);
	}
	return;
}


lista *MBC(grafo *g,lista *Q,lista *K)
{
	lista *C = copiaLista(Q);
    lista *nC = copiaLista(K);
	int v;
	if(K->tam!=0)
    {
		v = escolheVerticePertencenteK(K);
		lista *intersecao = achaIntersecao(K,g->E[v]);
		lista *uniao = achaUniao2(Q,v);
		C=achaUniao(C,MBC(g,uniao,intersecao));
		removeObjetoLista(K,v);
		C=achaUniao(C,MBC(g,Q,K));
    }
	
	lista *c = constroiLista();
	constroiBiParticoes(g,C,c);
	return C;
}

lista *MC(grafo *g,lista *Q,lista *K)
{
	lista *C = copiaLista(Q);
	int v;
	if(K->tam!=0)
    {
		v = escolheVerticePertencenteK(K);
		lista *intersecao = achaIntersecao(K,g->E[v]);
		lista *uniao = achaUniao2(Q,v);
		lista *C1=MC(g,uniao,intersecao);
		if(C1->tam > C->tam)
		{
			destroiLista(C);
			C = copiaLista(C1);
		}
		removeObjetoLista(K,v);
		lista *C2=MC(g,Q,K);
		if(C2->tam > C->tam)
		{
			destroiLista(C);
			C = copiaLista(C2);
		}
    }
	return C;
}

main(int argc,char *argv[])
{
	int i;	char arg = argv[1][0];
	grafo *g = constroiGrafo();
	lista *K = constroiLista();
	int tam = g->vTam;
	for(i=0;i<tam;i++)
		insereFrenteLista(K,i);
	lista *Q = constroiLista(); 

	if(arg=='g')
	{
		lista *ordem = lexBFS(g);

		if(ordem!=NULL)
		{
			if(cordal(g,ordem))
				printf("1\n");
			else
				printf("0\n");			
		}
		else
			printf("Erro :Lista de ordenacao nao Gerada\n");

	}
	else if(arg=='c')
	{
		printf("Maximum Clique:\n");
		mostraLista(MC(g,Q,K));
	}
	else if(arg=='b')
	{
		int j = 0;
		for(j=0;j<g->vTam;j++)
		{
			if((g->E[j]->tam > minBiClique))
			{
				minBiClique = g->E[j]->tam;
				maxBiClique = minBiClique;
				p1 = constroiLista();
				insereFrenteLista(p1,j);
				p2 = copiaLista(g->E[j]);
			}
		}

		MBC(g,Q,K);
		printf("Maximum BiClique:\n");
		printf("K1:\n");
		mostraLista(p1);
		printf("K2:\n");
		mostraLista(p2);
	}
	else
	{
		printf("Parametros Errados\n");
	}
}
