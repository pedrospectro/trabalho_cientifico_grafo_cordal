var vertice = 0;
var verticeInicio=-1;
var verticeFim=-1
var op=-1;
var vertexPlaced=0;
var str;
var pontoSet    = constroiPontoSet();
var retaSet 	= constroiRetaSet();
var edgeSet     = constroiPontoSet();

var escolhida = -1;
var cli = -2;
var ctx;


function modulo(n)
{
	if(n>=0)
		return n;
	else
		return -n;
}

function getId(str)
{
	return document.getElementById(str);
}

function constroiPonto(x,y)
{
	point = new Object();
	point.x = x;
	point.y = y;
	return point;
}

function centroReta(r)
{
	var pontoCentral = constroiPonto( (modulo(r.sp.x+r.ep.x))/2 ,(modulo(r.sp.y+r.ep.y))/2 );
	return pontoCentral;
}

function constroiReta(sPoint,ePoint)
{
	var r = new Object();
	r.sp = sPoint;
	r.ep = ePoint;
	r.mp = centroReta(r);
	return r;
}


function constroiPontoSet()
{
	pSet = new Object();
	pSet.tam = 0;
	pSet.array = new Array();
	return pSet;
}


function addPonto(pSet,p)
{
	pSet.array[pSet.tam] = p;
	pSet.tam=pSet.tam+1;
}

function constroiRetaSet()
{
	rSet = new Object();
	rSet.tam = 0;
	rSet.array = new Array();
	return rSet;
}

function addReta(rSet,r)
{
	rSet.array[rSet.tam]=r;
	rSet.tam=rSet.tam+1;
}

function dist(x0,x1)
{
	return (x0 - x1);
}

function distancia(x0,x1,y0,y1)
{
	var dx = dist(x0,x1);
	var dy = dist(y0,y1);
	return Math.sqrt( Math.pow(dx,2) + Math.pow(dy,2)  );
}

function findIt(evt) 
{
	//getId("mousex").value = evt.clientX-8;
	//getId("mousey").value = evt.clientY-8;
}

function operaVertice()
{
	op=1;
}


function operaAresta()
{
	op=2;
}


function buscaPonto(x,y,pS)
{
	var minDist  = 500000;
	var pos=-1;
	
	var i = 0;
	var tam =  pS.tam;

	while(i<tam)
	{
		var distS = distancia(pS.array[i].x,x,pS.array[i].y,y);				
		if(distS < minDist)
		{
			pos = i;
			minDist  = distS;
		}
		i=i+1;
	}

	if(minDist<13)
		return pos;
	else
		return -1;
}

function buscaEdge(edgeSet,u,v)
{
	var i = 0;
	var tam = edgeSet.tam;
	var arr = edgeSet.array;
	while(i<tam)
	{
		if( ((arr[i].x==u)&&(arr[i].y==v)) || ((arr[i].y==u)&&(arr[i].x==v)) )
			return 1;
		i=i+1;
	}
	return 0;
}

function findClick(evt)
{

	var canvas = getId("myCanvas");
	var rect = canvas.getBoundingClientRect();

	var x = evt.clientX-rect.left;
	var y = evt.clientY-rect.top;

	if(op==1)
	{
		var pt = constroiPonto(x,y);
		addPonto(pontoSet,pt);
		desenha();
		op=0;
	}
	else if(op==2)
	{		
		if(vertexPlaced==0)
		{		
			escolhida = buscaPonto(x,y,pontoSet);
			if(escolhida!=-1)
				vertexPlaced=vertexPlaced+1;
		}
		else
		{
			var destino = buscaPonto(x,y,pontoSet);
			if((destino!=escolhida)&&(destino!=-1))
			{
				if(!buscaEdge(edgeSet,destino,escolhida))
				{
					var ptU = constroiPonto(pontoSet.array[escolhida].x,pontoSet.array[escolhida].y);
					var ptV = constroiPonto(pontoSet.array[destino].x,pontoSet.array[destino].y);
					var r = constroiReta(ptU,ptV);
					var edge = constroiPonto(escolhida,destino);
					addPonto(edgeSet,edge);
					addReta(retaSet,r);
					desenha();
				}
				op=0;
				vertexPlaced=0;
			}			
		}
	}
}

function mostrarGrafo()
{
	str = pontoSet.tam + ' ';
	str = str + edgeSet.tam + ' ';
	var i=0;
	var tam=edgeSet.tam;
	while(i<tam)
	{
		str= str + edgeSet.array[i].x + " " + edgeSet.array[i].y + " ";
		i=i+1;	
	}
	alert(str);
}

function clearCanvas(context, canvas) 
{
	context.clearRect(0, 0, canvas.width, canvas.height);
	var w = canvas.width;
	canvas.width = 1;
	canvas.width = w;
}

function desenha()
{
	var c = getId("myCanvas");		
	ctx = c.getContext("2d");		
	ctx.font = "13px Arial";

	var pt;
	var i=0;
	var pList = pontoSet.array;
	var tam = pontoSet.tam;			

	while(i<tam)
	{		
		ctx.beginPath();
		pt = pList[i];
		ctx.arc(pt.x,pt.y,10,0,2*Math.PI);
		ctx.fillText(i,pt.x-2,pt.y+2);	
		ctx.stroke();
		i=i+1;				
	}

	i=0;
	var rList = retaSet.array;
	tam = retaSet.tam;			

	//alert(tam);	
	while(i<tam)
	{					
		ctx.moveTo(rList[i].sp.x,rList[i].sp.y);
		ctx.lineTo(rList[i].ep.x,rList[i].ep.y);
		i=i+1;				
	}
	ctx.stroke();
}


function geraTextField(str)
{
 	str = pontoSet.tam + ' ';
	str = str + edgeSet.tam + ' ';
	var i=0;
	var tam=edgeSet.tam;
	while(i<tam)
	{
		str= str + edgeSet.array[i].x + " " + edgeSet.array[i].y + " ";
		i=i+1;	
	}
	var data = document.getElementById("micropost_content");
	data.value=str;
	return str;
}
